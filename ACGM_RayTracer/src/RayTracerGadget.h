#pragma once

#include <HIRO/Gadget.h>

#include <HIRO_DRAW/renderers/RasterRenderer.h>

namespace acgm {
	class Scene;
	class SceneImporter;
}


//! Visual part of the RayTracer HIRO module.
class RayTracerGadget
  : public hiro::Gadget
{
public:
  //! Construct with a HIRO resource.
  explicit RayTracerGadget(const hiro::Resource *res);

  void Initialize() override;
  void GenerateGui(hiro::GuiGenerator& gui) override;
  void Update(const float time_delta) override;

  void RenderScene();
private: 
  //! Structure specifying how the raster should be rendered.
  hiro::draw::PRasterStyle style_;

  //gui Elements
  hiro::gui::Droplist* m_sceneSelector;
  hiro::gui::SlidingInt* m_maxReflectionSelector;
  hiro::gui::SlidingInt* m_maxRefractionSelector;
  hiro::gui::Button* m_renderButton;

  //scene
  std::shared_ptr<acgm::SceneImporter> m_importer;
  std::shared_ptr<acgm::Scene> m_scene;
};
