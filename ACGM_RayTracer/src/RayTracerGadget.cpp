#include "RayTracerResource.h"
#include "RayTracerGadget.h"
#include <ACGM_RayTracer_lib/Mesh.h>
#include <ACGM_RayTracer_lib/DirectionalLight.h>
#include <ACGM_RayTracer_lib/PointLight.h>
#include <ACGM_RayTracer_lib/SceneImporter.h>



RayTracerGadget::RayTracerGadget(const hiro::Resource *res)
  : hiro::Gadget(res)
{
}

void RayTracerGadget::Initialize()
{
  hiro::Gadget::Initialize();
  style_ = std::make_shared<hiro::draw::RasterStyle>();
  style_->use_nearest_filtering = true;

  const RayTracerResource* res = GetResource<RayTracerResource>();
  auto a = res->GetRenderer();
  AddRenderer(a,style_);

  m_importer = std::make_shared<acgm::SceneImporter>();
}

void RayTracerGadget::GenerateGui(hiro::GuiGenerator & gui)
{
	//scene selector droplist
	m_sceneSelector = gui.AddDroplist("Scenes")
		->AddItemsIndexed({ "scene0.txt", "scene1.txt", "scene2.txt", "scene3.txt"
			, "scene4.txt", "scene5.txt", "scene6.txt", "scene7.txt", "scene8.txt" })
		->Set(0);

	m_maxReflectionSelector = gui.AddSlidingInt("Max Reflection")
		->SetMin(0)->SetMax(5)->SetStep(1)->Set(2);

	m_maxRefractionSelector = gui.AddSlidingInt("Max Refraction")
		->SetMin(0)->SetMax(5)->SetStep(1)->Set(2);

	//button to render
	m_renderButton = gui.AddButton("Render")
		->SetCaption("Render Scene")->Subscribe([this](const hiro::gui::Button *button) {
		this->RenderScene();
	});
}

void RayTracerGadget::Update(const float time_delta)
{
	hiro::Gadget::Update(time_delta);
}

void RayTracerGadget::RenderScene()
{
	//load scene from file
	if (m_importer->Import(m_sceneSelector->GetText()))
	{
		m_scene = m_importer->GetScene();

		if (m_scene)
		{
			acgm::RenderOptions ro = m_importer->GetRenderOptions();
			GetResource<RayTracerResource>()->GetRenderer()->SetResolution(ro.resolution);
			m_scene->Raytrace(*GetResource<RayTracerResource>()->GetRenderer(), m_maxReflectionSelector->Get(), m_maxRefractionSelector->Get());
		}
			
	}
	
	
}
