#include "pch.h"

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/geometric.hpp>
#include <glm/gtc/epsilon.hpp>

#include <ACGM_RayTracer_lib/DirectionalLight.h>
#include <ACGM_RayTracer_lib/SpotLight.h>



//Directional Light really just returns member variable, no need to test that

TEST(LightTests, PointLightIntensityTest)
{
	auto light = std::make_shared<acgm::PointLight>(1.0f, glm::vec3(0.0f,0.0f,0.0f), 10.0f, 0.0f, 1.0f);

	//make sure intensity goes down with distance
	EXPECT_TRUE(light->GetIntensityInPoint(glm::vec3(0.0f, 0.0f, 10.f)) > light->GetIntensityInPoint(glm::vec3(0.0f, 0.0f, 11.f)));
	EXPECT_TRUE(light->GetIntensityInPoint(glm::vec3(0.0f, 0.0f, 30.f)) > light->GetIntensityInPoint(glm::vec3(0.0f, 0.0f, 35.f)));
	EXPECT_TRUE(light->GetIntensityInPoint(glm::vec3(0.0f, 0.0f, 0.1f)) > light->GetIntensityInPoint(glm::vec3(0.3f, 0.0f, 0.0f)));

	//intensity must be between 1 and 0, also close point should have high intensity, distant point low intensity
	EXPECT_TRUE(light->GetIntensityInPoint(glm::vec3(0.0f, 0.0f, 0.1f)) < 1.f);
	EXPECT_TRUE(light->GetIntensityInPoint(glm::vec3(0.0f, 0.0f, 30.f)) < 0.3f);

	EXPECT_TRUE(light->GetIntensityInPoint(glm::vec3(0.0f, 0.0f, 0.1f)) > 0.9f);
	EXPECT_TRUE(light->GetIntensityInPoint(glm::vec3(0.0f, 0.0f, 30.f)) > 0.f);
}

TEST(LightTests, SpotLightIntensityTest)
{
	auto light = std::make_shared<acgm::SpotLight>(1.0f, glm::vec3(0.0f, 0.0f, 5.0f), 10.0f, 0.0f, 1.0f, glm::vec3(0.0f,0.0f,-5.0f), 45.0f, 3.0f);

	//intensity is tested in PointLight test

	//check inside cutoff
	EXPECT_TRUE(light->GetIntensityInPoint(glm::vec3(0.0f, 0.0f, 0.0f)) > 0.5f);

	//check on edge
	EXPECT_TRUE(light->GetIntensityInPoint(glm::vec3(4.9f, 0.0f, 0.0f)) < 0.5f);
	EXPECT_TRUE(light->GetIntensityInPoint(glm::vec3(4.9f, 0.0f, 0.0f)) > 0.f);


	//check outside
	EXPECT_TRUE(light->GetIntensityInPoint(glm::vec3(5.2f, 0.0f, 0.0f)) == 0.f);

	//point on side of spot light, outside the cone
	EXPECT_TRUE(light->GetIntensityInPoint(glm::vec3(0.2f, 0.0f, 5.0f)) == 0.f);

}
