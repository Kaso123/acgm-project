#include "pch.h"

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/geometric.hpp>
#include <glm/gtc/epsilon.hpp>

#include <ACGM_RayTracer_lib/PhongShader.h>
#include <ACGM_RayTracer_lib/CheckerShader.h>




TEST(ShaderTests, PhongShaderTest)
{
	auto shader = std::make_shared<acgm::PhongShader>(cogs::Color3f(1.0f,0.1f,0.1f), 90.f, 0.2f, 0.8f, 0.8f,0.f,0.f,0.f);

	acgm::ShaderInput input;
	input.intensity = 0.8f;
	input.normal = glm::vec3(0.f, 1.f, 0.f);
	input.dirToCamera = glm::normalize(glm::vec3(1.f,1.f,0.f));
	input.dirToLight = glm::normalize(glm::vec3(-1.f, 1.f, 0.f));

	auto specColor = shader->CalculateColor(input);

	input.dirToLight = glm::normalize(glm::vec3(-1.f, 0.f, 0.f));

	auto baseColor = shader->CalculateColor(input);

	input.dirToLight = glm::normalize(glm::vec3(-1.f, -0.5f, 0.f));

	auto shadowColor = shader->CalculateColor(input);
	
	
	EXPECT_TRUE(shadowColor.r < baseColor.r);
	EXPECT_TRUE(shadowColor.r < specColor.r);
	EXPECT_TRUE(baseColor.r < specColor.r);
	EXPECT_TRUE(baseColor.g < specColor.g);
	EXPECT_TRUE(baseColor.b < specColor.b);

	//without specular, there should only be red
	EXPECT_TRUE(baseColor.b < 0.1f);
	EXPECT_TRUE(baseColor.g < 0.1f);
	EXPECT_TRUE(shadowColor.b < 0.1f);
	EXPECT_TRUE(shadowColor.g < 0.1f);

	//specular should have whiter color = higher green and blue
	EXPECT_TRUE(specColor.b > 0.5f);
	EXPECT_TRUE(specColor.g > 0.5f);
	
}

TEST(ShaderTests, CheckerShaderTest)
{
	//only ambient shaders
	auto redShader = std::make_shared<acgm::PhongShader>(cogs::color::RED, 90.f, 1.f, 0.f, 0.f, 0.f, 0.f, 0.f);
	auto blueShader = std::make_shared<acgm::PhongShader>(cogs::color::BLUE, 90.f, 1.f, 0.f, 0.f, 0.f, 0.f, 0.f);

	auto checker = std::make_shared<acgm::CheckerShader>(1.f, redShader, blueShader);

	acgm::ShaderInput input;
	input.intensity = 1.f;
	input.normal = glm::vec3(0.f, 1.f, 0.f);
	input.dirToCamera = glm::normalize(glm::vec3(1.f, 1.f, 0.f));
	input.dirToLight = glm::normalize(glm::vec3(-1.f, 0.f, 0.f));
	
	
	input.point = glm::vec3(0.1f, 0.5f, 0.2f);
	EXPECT_TRUE(checker->CalculateColor(input) == cogs::color::RED);

	input.point = glm::vec3(1.1f, 0.5f, 0.2f);
	EXPECT_TRUE(checker->CalculateColor(input) == cogs::color::BLUE);

	input.point = glm::vec3(1.5f, 1.7f, 0.2f);
	EXPECT_TRUE(checker->CalculateColor(input) == cogs::color::RED);

	input.point = glm::vec3(-0.5f, 0.7f, 0.2f);
	EXPECT_TRUE(checker->CalculateColor(input) == cogs::color::BLUE);



}
