#include "pch.h"

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/geometric.hpp>


#include <ACGM_RayTracer_lib/Ray.h>
//#include <ACGM_RayTracer_lib/Model.h>
#include <ACGM_RayTracer_lib/Plane.h>
#include <ACGM_RayTracer_lib/Mesh.h>
#include <ACGM_RayTracer_lib/Sphere.h>


TEST(IntersectionTests, RayPlaneIntersectionTest)
{
	using namespace glm;

	struct RayPlane {
		vec3 point;
		vec3 dir;
		vec3 planePoint;
		vec3 planeNormal;
		float res;
	};
	
 
	std::vector<RayPlane> vec;
	vec.reserve(4);

	//straight ray to plane
	vec.emplace_back(RayPlane{ vec3(0.0f,0.0f,0.0f), vec3(0.0f,0.0f,1.0f), //ray
		vec3(0.0f,0.0f,5.0f), vec3(0.0f,0.0f,-1.0f), //plane
		5.0f });

	//parallel plane
	vec.emplace_back(RayPlane{ vec3(0.0f,0.0f,0.0f), vec3(0.0f,0.0f,1.0f), //ray
		vec3(0.2f,0.0f,5.0f), vec3(1.0f,0.0f,0.0f), //plane
		0.0f });

	//angled plane
	vec.emplace_back(RayPlane{ vec3(0.0f,0.0f,0.0f), vec3(0.0f,0.0f,1.0f), //ray
		vec3(0.0f,0.0f,20.0f), vec3(0.3f,0.5f,0.7f), //plane
		20.0f });

	//plane behind camera
	vec.emplace_back(RayPlane{ vec3(0.0f,0.0f,0.0f), vec3(0.0f,0.0f,1.0f), //ray
		vec3(0.0f,0.0f,-1.0f), vec3(0.3f,0.5f,0.7f), //plane
		-1.0f });

	for (auto& test : vec)
	{
		auto ray = std::make_shared<acgm::Ray>(test.point, test.dir);
		auto plane = std::make_shared<acgm::Plane>(test.planeNormal, test.planePoint);

		auto hr = plane->Intersect(*ray);

		if (test.res == 0.0f)
			EXPECT_TRUE(!hr.has_value());
		else
		{
			EXPECT_TRUE(hr.has_value());
			EXPECT_TRUE(glm::epsilonEqual<float>(hr->dist, test.res,glm::epsilon<float>()));
		}

	}
}

TEST(IntersectionTests, RaySphereIntersectionTest)
{
	using namespace glm;

	struct RaySphere {
		vec3 point;
		vec3 dir;
		vec3 spherePoint;
		float sphereRadius;
		bool res;
	};

	std::vector<RaySphere> vec;
	vec.reserve(4);

	//straight ray to sphere
	vec.emplace_back(RaySphere{ vec3(0.0f,0.0f,0.0f), vec3(0.0f,0.0f,1.0f), //ray
		vec3(0.0f,0.0f,5.0f), 2.0f, //sphere
		true });

	//sphere behind ray origin
	vec.emplace_back(RaySphere{ vec3(0.0f,0.0f,0.0f), vec3(0.0f,0.0f,1.0f), //ray
		vec3(0.0f,0.0f,-5.0f), 2.0f, //sphere
		false });

	//hitting sphere on edge
	vec.emplace_back(RaySphere{ vec3(0.0f,0.0f,0.0f), vec3(2.99f, 0.0f, 10.0f), //ray
		vec3(0.0f,0.0f,10.0f), 3.0f, //sphere
		true });

	//ray going outside the sphere
	vec.emplace_back(RaySphere{ vec3(0.0f,0.0f,0.0f), vec3(3.2f, 0.0f, 10.0f), //ray
		vec3(0.0f,0.0f,10.0f), 3.0f, //sphere
		false });

	for (auto& test : vec)
	{
		auto ray = std::make_shared<acgm::Ray>(test.point, test.dir);
		auto sphere = std::make_shared<acgm::Sphere>(test.spherePoint, test.sphereRadius);

		auto hr = sphere->Intersect(*ray);

		EXPECT_EQ(test.res, hr.has_value() && hr->dist > 0.0f);
	}
}

TEST(IntersectionTests, RayTriangleIntersectionTest)
{
	using namespace glm;

	struct RayTriangle {
		vec3 point;
		vec3 dir;
		vec3 a;
		vec3 b;
		vec3 c;
		bool res;
	};

	std::vector<RayTriangle> vec;
	vec.reserve(3);

	//center of triangle
	vec.emplace_back(RayTriangle{ vec3(0.0f,0.0f,0.0f), vec3(0.0f,0.0f,1.0f), //ray
		vec3(0.0f,1.0f,5.0f), vec3(1.0f,0.0f,5.0f), vec3(-1.0f,-1.0f,5.0f), //triangle
		true });

	//point of triangle
	vec.emplace_back(RayTriangle{ vec3(0.0f,0.0f,0.0f), vec3(0.0f,0.0f,1.0f), //ray
		vec3(0.0f,0.0f,5.0f), vec3(1.0f,5.0f,5.0f), vec3(5.0f,1.0f,5.0f), //triangle
		true });

	//edge of triangle
	vec.emplace_back(RayTriangle{ vec3(0.0f,0.0f,0.0f), vec3(0.0f,0.0f,1.0f), //ray
		vec3(0.0f,1.0f,5.0f), vec3(0.0f,-1.0f,5.0f), vec3(4.0f,0.0f,5.0f), //triangle
		true });



	auto helpMesh = std::make_shared<acgm::Mesh>();

	for (auto& test : vec)
	{
		auto ray = std::make_shared<acgm::Ray>(test.point, test.dir);

		auto hr = helpMesh->TriangleIntersect(*ray,test.a,test.b,test.c);

		EXPECT_EQ(test.res, hr.has_value() && hr->dist > 0.0f);
	}
}
