#include <ACGM_RayTracer_lib/Plane.h>
#include <glm/geometric.hpp>
#include <cmath>

// #TODO Implement your functionality here

acgm::Plane::Plane(const glm::vec3& normal, const glm::vec3& point)
	:m_normal(glm::normalize(normal)), m_point(point)
{
}

std::optional<acgm::HitResult> acgm::Plane::Intersect(const acgm::Ray& ray) const
{
	float x = glm::dot(m_point - ray.Origin(), m_normal);
	float y = glm::dot(ray.Direction(), m_normal);

	//for parallel plane, don't divide by zero
	if (glm::abs(y) < ray.GetBias())
		return std::nullopt;

	return acgm::HitResult{ x / y , m_normal};
}
