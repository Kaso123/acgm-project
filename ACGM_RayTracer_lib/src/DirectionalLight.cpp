#include <ACGM_RayTracer_lib/DirectionalLight.h>
#include <glm/geometric.hpp>
#include <cmath>

acgm::DirectionalLight::DirectionalLight(float intensity, glm::vec3 direction)
	:acgm::Light(intensity), 
	m_direction(direction)
{
}

float acgm::DirectionalLight::GetIntensityInPoint(const glm::vec3 & point) const
{
	return m_intensity;
}

glm::vec3 acgm::DirectionalLight::GetDirectionToLight(const glm::vec3 & point) const
{
	//reversed light direction = dir to light from point
	return glm::normalize(-m_direction);
}

float acgm::DirectionalLight::GetDistanceToLight(const glm::vec3 & point) const
{
	//return high value, simulating sun distance
	return std::numeric_limits<float>::max();
}

