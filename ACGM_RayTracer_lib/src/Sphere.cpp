#include <glm/geometric.hpp>
#include <glm/gtc/epsilon.hpp>
#include <ACGM_RayTracer_lib/Sphere.h>
#include <cmath>


acgm::Sphere::Sphere(const glm::vec3 &position, float radius)
	:m_position(position), m_radius(radius)
{
}

std::optional<acgm::HitResult> acgm::Sphere::Intersect(const acgm::Ray &ray) const
{
  glm::vec3 oo = ray.Origin() - m_position;

  float A = glm::dot(ray.Direction(), ray.Direction());
  float B = -2.0f * glm::dot(oo, ray.Direction());
  float C = glm::dot(oo, oo) - m_radius * m_radius;
  float D = B * B - 4.0f * A * C;

  if (D < 0)
  {
    return std::nullopt;
  }

  float sD = glm::sqrt(D);

  float t1 = 0.5 * (B + sD) / A;
  if (t1 < ray.GetBias())
  {
    t1 = INFINITY;
  }

  float t2 = 0.5 * (B - sD) / A;
  if (t2 < ray.GetBias())
  {
    t2 = INFINITY;
  }

  const float t = glm::min(t1, t2);
  if (glm::isinf(t) || t < 0.0f)
  {
    return std::nullopt;
  }

  //normal = vector from center of sphere to hit point
  return acgm::HitResult{ t, glm::normalize(ray.GetPoint(t) - m_position) }; 
}

const glm::vec3 &acgm::Sphere::GetPosition() const
{
  return m_position;
}

float acgm::Sphere::GetRadius() const
{
  return m_radius;
}

