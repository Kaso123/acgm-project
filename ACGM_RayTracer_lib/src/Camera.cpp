#include <ACGM_RayTracer_lib/Camera.h>
#include <glm/geometric.hpp>
#include <glm/gtc/constants.hpp>


acgm::Camera::Camera(const glm::vec3 &position, const glm::vec3 &up_direction, const glm::vec3 &forward_direction,
	float z_near, float z_far, float fov_y_rad)
  : m_position(position), m_upVector(up_direction), m_forwardVector(forward_direction)
	,m_nearClippingPlane(z_near), m_farClippingPlane(z_far), m_fovY(fov_y_rad)
{
	m_fovX = m_fovY;
}

const glm::vec3 &acgm::Camera::Position() const
{
  return m_position;
}

glm::vec3 acgm::Camera::Direction() const
{
	return glm::normalize(m_forwardVector);
}

glm::vec3 acgm::Camera::UpVector() const
{
	return glm::normalize(glm::cross(Direction(), RightVector()));
}

glm::vec3 acgm::Camera::RightVector() const
{
	//return glm::normalize(glm::cross(Direction(),UpVector()));
	return glm::normalize(glm::cross(Direction(), m_upVector));
}
