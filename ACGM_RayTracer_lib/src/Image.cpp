#define STB_IMAGE_IMPLEMENTATION
#include <ACGM_RayTracer_lib/stb_image.h>
#include <ACGM_RayTracer_lib/Image.h>

// #TODO Implement your functionality here

acgm::Image::Image(std::string& file)
{
	m_image = stbi_load(file.c_str(), &m_width, &m_height, &m_bpp, 3);
}

acgm::Image::~Image()
{
	stbi_image_free(m_image);
}

cogs::Color3f acgm::Image::GetColorAt(const glm::vec2 & uvs) const
{
	// find pixel position in array -> 3*(y*width+x)
	int pixel = m_bpp * ((UvToInt(uvs.y, false) * m_width) + UvToInt(uvs.x));
	return cogs::Color3b(m_image[pixel], m_image[pixel + 1], m_image[pixel + 2]);
}

int acgm::Image::UvToInt(float num, bool x /*= true*/) const
{
	return int(glm::round( num * (x ? m_width - 1  : m_height - 1)));
}

