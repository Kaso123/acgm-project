#include <ACGM_RayTracer_lib/SpotLight.h>
#include <glm/geometric.hpp>
#include <glm/trigonometric.hpp>
#include <glm/gtc/constants.hpp>


acgm::SpotLight::SpotLight(float intensity, glm::vec3 position, float range, float linAtt, float quadAtt, glm::vec3 direction, float cutoff, float exponent)
	:acgm::PointLight(intensity, position, range, linAtt, quadAtt), m_direction(direction), m_cutoff_angle(cutoff), m_exponent(exponent)
{
}

float acgm::SpotLight::GetIntensityInPoint(const glm::vec3 & point) const
{
	float base = PointLight::GetIntensityInPoint(point);
	
	float angle = GetAngleBetween(GetDirectionToLight(point)*(-1.0f), m_direction);

	if (angle > m_cutoff_angle) //no light outside cone
		return 0.0f;

	float decay = 1 - glm::pow((angle / m_cutoff_angle), m_exponent);

	return base * decay;
}

float acgm::SpotLight::GetAngleBetween(const glm::vec3 & dir1, const glm::vec3 & dir2) const
{
	//returns angle in degrees (cutoff_angle is provided in degrees)
	return glm::acos(glm::dot(dir1, dir2) / glm::sqrt(glm::dot(dir1, dir1) * glm::dot(dir2, dir2))) * 180.0f / glm::pi<float>();
}
