#include <ACGM_RayTracer_lib/Mesh.h>
#include <COGS/PointCloud.h>
#include <glm/geometric.hpp>
#include <glm/gtc/constants.hpp>
#include <cmath>


acgm::Mesh::Mesh(const std::string& filename, const glm::mat4& transform)
{
	m_mesh.Import(filename);
	m_transformMatrix.Set(transform);	
}

std::optional<acgm::HitResult> acgm::Mesh::Intersect(const acgm::Ray& ray) const
{
	//return null, if mesh has no faces
	if (!m_mesh.HasFaces())
		return std::nullopt;

	//initialize result variables
	HitResult result{ std::numeric_limits<float>::max(), glm::vec3(0.0f,0.0f,0.0f) };

	//cycle all faces
	for (const auto &face : m_mesh.faces->GetFaces())
	{
		//3 points of triangle
		glm::vec3 a, b, c;
		
		//apply transformation
		a = glm::vec3(m_transformMatrix.GetMat4()*(glm::vec4(*(m_mesh.points->GetPositions() + face.x), 1.0f)));
		b = glm::vec3(m_transformMatrix.GetMat4()*(glm::vec4(*(m_mesh.points->GetPositions() + face.y), 1.0f)));
		c = glm::vec3(m_transformMatrix.GetMat4()*(glm::vec4(*(m_mesh.points->GetPositions() + face.z), 1.0f)));

		auto triangleHit = TriangleIntersect(ray, a, b, c);
		
		//compare triangle distance to actual closest hit
		if (triangleHit.has_value() && result.dist > triangleHit->dist)
		{
			result = triangleHit.value();
		}
	}

	//if no triangle was hit, return null
	if (result.dist < std::numeric_limits<float>::max())
	{
		return result;
	} 
	return std::nullopt;
}

std::optional<acgm::HitResult> acgm::Mesh::TriangleIntersect(const acgm::Ray & ray, const glm::vec3 & a, const glm::vec3 & b, const glm::vec3 & c) const
{
	//edge vectors
	glm::vec3 edge1 = b - a;
	glm::vec3 edge2 = c - a;

	//normal 
	glm::vec3 triangleNormal = glm::normalize(glm::cross(edge1, edge2));
	glm::vec3 q = glm::cross(ray.Direction(), edge2);
	float x = glm::dot(edge1, q);

	//return if back side of face was hit
	/*if (glm::dot(normal, ray.Direction()) >= 0 || (std::abs(x) <= ray->GetBias()))
		return std::nullopt;*/

		//barycentric coords
	glm::vec3 s = (ray.Origin() - a) / x;
	glm::vec3 r = glm::cross(s, edge1);

	float b0, b1, b2;
	b0 = glm::dot(s, q);
	b1 = glm::dot(r, ray.Direction());
	b2 = 1.0f - b0 - b1;

	//check if inside triangle
	if ((b0 < 0.0f) || (b1 < 0.0f) || (b2 < 0.0f))
		return std::nullopt;

	float t = glm::dot(edge2, r);
	if (t >= 0.0f)
	{
		return acgm::HitResult{ t, triangleNormal };
	}
	return std::nullopt;
}
