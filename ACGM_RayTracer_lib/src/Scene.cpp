#include <ACGM_RayTracer_lib/Scene.h>
#include <ACGM_RayTracer_lib/Ray.h>
#include <ACGM_RayTracer_lib/Camera.h>
#include <ACGM_RayTracer_lib/Plane.h>
#include <ACGM_RayTracer_lib/Mesh.h>
#include <ACGM_RayTracer_lib/Image.h>
#include <ACGM_RayTracer_lib/DirectionalLight.h>
#include <ACGM_RayTracer_lib/PointLight.h>
#include <iostream>
#include <limits>
#include <omp.h>
#include <glm/gtx/vector_angle.hpp>



acgm::Scene::Scene(std::shared_ptr<acgm::Camera> camera, std::shared_ptr<acgm::Light> light, std::vector<std::shared_ptr<acgm::Model>> models, 
	std::string& image_file, glm::vec3 envUp, glm::vec3 envSeam, float airRefrIndex)
	: m_camera(camera), m_light(light), m_models(models), m_envUp(glm::normalize(envUp)), m_envSeam(glm::normalize(envSeam)), m_refractionIndex(airRefrIndex)
{
	if (!image_file.empty())
		m_image = std::make_shared<acgm::Image>(image_file);
	else m_image = nullptr;
}

void acgm::Scene::Raytrace(hiro::draw::RasterRenderer &renderer, int maxRefl, int maxRefr) const
{
    
  //loop through renderer pixels
	#pragma omp parallel for
	for (int x = 0; x < renderer.GetResolution().x; x++)
	{
		for (int y = 0; y < renderer.GetResolution().y; y++)
		{
			cogs::Color3f color = cogs::color::BLACK; //default color without an object
			
			//create ray for pixel[x,y]
			std::shared_ptr<acgm::Ray> temp_ray = CameraRay(x, y, m_camera, renderer.GetResolution().x, renderer.GetResolution().y);

			renderer.SetPixel(x, y, RecursiveCastRay(temp_ray, 0, 0, maxRefl, maxRefr));
		}
	}
}

std::shared_ptr<acgm::Ray> acgm::Scene::CameraRay(int x, int y, std::shared_ptr<acgm::Camera> camera, int imageWidth, int imageHeight) const
{
	float u, v, du, dv;
	glm::vec3 dir;

	//initialize parameters
	v = glm::tan(camera->FovY() * 0.5f);
	u = -1.0f * glm::tan(camera->FovX() * 0.5f);

	//get increments for one pixel
	dv = 2.0f * glm::tan(camera->FovY() * 0.5f) / (float)imageHeight;
	du = 2.0f * glm::tan(camera->FovX() * 0.5f) / (float)imageWidth;

	//move according to selected pixel
	v -= dv * y;
	u += du * x;

	//create direction vector
	dir = glm::normalize(camera->Direction() + u * camera->RightVector() + v * camera->UpVector());
	return std::make_shared<acgm::Ray>(camera->Position(), dir);
}

std::shared_ptr<acgm::Ray> acgm::Scene::ReflectionRay(glm::vec3 ray, glm::vec3 normal, glm::vec3 origin) const
{
	glm::vec3 dir = glm::normalize(2 * (glm::dot(-ray,normal))*normal + ray);

	return std::make_shared<acgm::Ray>(origin, dir);
}

std::shared_ptr<acgm::Ray> acgm::Scene::RefractionRay(glm::vec3 ray, glm::vec3 normal, glm::vec3 origin, float refrIndex) const
{

	float cosTheta1 = glm::dot(ray, normal);
	glm::vec3 n = normal;

	float indexFrom, indexTo;
	
	if (cosTheta1 < 0) 
	{
		cosTheta1 = -cosTheta1;
		indexFrom = m_refractionIndex;
		indexTo = refrIndex;
	}
	else //going from inside the model, switch normals
	{
		indexTo = m_refractionIndex;
		indexFrom = refrIndex;
		n = -n;
	}

	float index = indexFrom / indexTo;
	float theta = 1 - index * index * (1 - cosTheta1 * cosTheta1);

	if (theta < 0) //total internal reflection
		return nullptr;

	glm::vec3 dir = index * ray + (index*cosTheta1 - sqrtf(theta)) * n;
	return std::make_shared<acgm::Ray>(origin, dir);
}

cogs::Color3f acgm::Scene::RecursiveCastRay(std::shared_ptr<acgm::Ray> ray, int reflCount, int refrCount, int maxRefl, int maxRefr) const
{
	HitResult hit; //this holds closest found hit
	hit.dist = std::numeric_limits<float>::max();

	//cycle all models and pick the closest
	for (const auto &model : m_models)
	{
		auto temp = model->Intersect(*ray);
		if (temp.has_value() && temp->dist > 0.0 && temp->dist < hit.dist)
		{
			if (temp->dist < m_camera->FarClippingPlane() && temp->dist > m_camera->NearClippingPlane()) //only render inside clipping planes
			{
				hit = temp.value();
				hit.model = model; //pointer to model (needed to calculate color later)
			}
		}
	}

	//didn't hit anything inside clipping planes, set pixel to black and go to next
	if (hit.dist > m_camera->FarClippingPlane())
	{
		if (!m_image) //no enviro image
		{
			return cogs::color::BLACK;
		}
		else //get color from environment map
		{
			glm::vec2 uvs = GetUVs(ray);

			return m_image->GetColorAt(uvs);
		}
	}

	//point in which we hit something
	glm::vec3 hitPoint = ray->GetPoint(hit.dist);

	//cycle all lights and add their intensities in point (only one light for now)
	std::shared_ptr<acgm::Ray> light_ray = std::make_shared<acgm::Ray>(hitPoint, m_light->GetDirectionToLight(hitPoint));

	//get distance from hit point to light
	float distToLight = m_light->GetDistanceToLight(hitPoint);
	bool lightHit = true;

	//cycle all models and if any is closer than light, dont add light intensity
	for (const auto &model : m_models)
	{
		auto hit = model->Intersect(*light_ray);
		if (hit->dist < distToLight && hit->dist > ray->GetBias())
		{
			//light obstructed by model
			lightHit = false;
			break;
		}
	}

	float intensity = 0;

	//increase light intensity if light is visible
	if (lightHit)
		intensity += m_light->GetIntensityInPoint(hitPoint);

	//create struct for shader
	ShaderInput input{ hitPoint, hit.normal, ray->Direction()*(-1.0f), light_ray->Direction(), intensity, !lightHit };

	//colors for mixing in reflections and refractions
	cogs::Color3f baseColor = hit.model->Color(input) * (1 - hit.model->Glossiness(hitPoint) - hit.model->Transparency(hitPoint));
	cogs::Color3f reflColor;
	cogs::Color3f refrColor;

	//reflection color
	if (reflCount >= maxRefl || hit.model->Glossiness(hitPoint) < 0.1f)
	{
		reflColor = hit.model->Color(input) * hit.model->Glossiness(hitPoint);
	}
	else
	{
		//cast another reflection ray
		std::shared_ptr<acgm::Ray> reflRay = ReflectionRay(ray->Direction(), hit.normal, hitPoint);
		reflColor = hit.model->Glossiness(hitPoint) * RecursiveCastRay(reflRay, reflCount + 1, refrCount, maxRefl, maxRefr);
	}

	//refraction color
	if (refrCount >= maxRefr || hit.model->Transparency(hitPoint) < 0.1f)
	{
		refrColor = hit.model->Color(input) * hit.model->Transparency(hitPoint);
	}
	else
	{
		//get refraction ray
		//added half of bias, so the new ray doesn't hit the same wall
		std::shared_ptr<acgm::Ray> refrRay = RefractionRay(ray->Direction(), hit.normal, hitPoint+ray->Direction()*0.00005f, hit.model->RefractionIndex(hitPoint));		
		
		if (!refrRay) //total internal reflection
		{
			if (refrCount < maxRefr)
			{
				std::shared_ptr<acgm::Ray> reflRay = ReflectionRay(ray->Direction(), hit.normal, hitPoint);
				refrColor = hit.model->Transparency(hitPoint) * RecursiveCastRay(reflRay, reflCount, refrCount + 1, maxRefl, maxRefr);
			}
			else refrColor = hit.model->Transparency(hitPoint) * hit.model->Color(input);
		}
			
		else
			refrColor = hit.model->Transparency(hitPoint) * RecursiveCastRay(refrRay, reflCount, refrCount + 1, maxRefl, maxRefr);
	}
	return baseColor + reflColor + refrColor;
}

glm::vec2 acgm::Scene::GetUVs(std::shared_ptr<acgm::Ray> ray) const
{
	float fLong,fLat;

	fLat = std::acos(glm::dot(ray->Direction(), m_envUp));
	
	glm::vec3 x = glm::normalize(ray->Direction() - glm::dot(ray->Direction(), m_envUp) * m_envUp);
	fLong = glm::orientedAngle(x, m_envSeam, m_envUp);

	//clamping to <0,1>
	return glm::vec2((fLong + glm::pi<float>()) / (glm::pi<float>() * 2), fLat/ glm::pi<float>());
}
