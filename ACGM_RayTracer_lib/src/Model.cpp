#include <ACGM_RayTracer_lib/Model.h>
#include <ACGM_RayTracer_lib/Shader.h>



acgm::Model::Model()
{
}

const cogs::Color3f acgm::Model::Color(acgm::ShaderInput& input) const
{
  return m_shader->CalculateColor(input);
}

