#include <ACGM_RayTracer_lib/Ray.h>
#include <ACGM_RayTracer_lib/Plane.h>
#include <ACGM_RayTracer_lib/Model.h>


acgm::Ray::Ray(glm::vec3 origin, glm::vec3 direction)
	:m_origin(origin), m_direction(glm::normalize(direction))
{
}

const glm::vec3 acgm::Ray::GetPoint(float dist) const
{
	return m_origin + m_direction * (dist - GetBias());
}
