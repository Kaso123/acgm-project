#include <ACGM_RayTracer_lib/PhongShader.h>
#include <glm/trigonometric.hpp>



acgm::PhongShader::PhongShader(cogs::Color3f color, float shininess, float ambient, float diffuse, float specular, float gloss, float trans, float refrIndex)
	:m_color(color), m_shininess(shininess), m_ambient(ambient), m_diffuse(diffuse), m_specular(specular), m_gloss(gloss), m_trans(trans), m_refrIndex(refrIndex)
{
}

cogs::Color3f acgm::PhongShader::CalculateColor(const acgm::ShaderInput& input)
{
	cogs::Color3f ambient = m_color * m_ambient;
	if (input.intensity <= 0)
		return ambient;

	//cogs::Color3f diffuse = m_color * m_diffuse * input.intensity * glm::cos(GetAngleBetween(input.normal,input.dirToLight));
	cogs::Color3f diffuse = m_color * m_diffuse * input.intensity * glm::dot(input.normal, input.dirToLight);

	//blinn-phong - half vector between direction to light and to camera
	float cos = glm::dot(glm::normalize(input.dirToLight + input.dirToCamera), input.normal);

	cogs::Color3f specular = cogs::color::WHITE * m_specular * input.intensity * glm::pow(cos,m_shininess);

	return ambient + diffuse + specular;
}

float acgm::PhongShader::GetAngleBetween(const glm::vec3 & dir1, const glm::vec3 & dir2) const
{
	//returns in radians
	return glm::acos(glm::dot(dir1, dir2) / glm::sqrt(glm::dot(dir1, dir1) * glm::dot(dir2, dir2)));
}