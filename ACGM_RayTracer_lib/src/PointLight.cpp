#include <ACGM_RayTracer_lib/PointLight.h>
#include <glm/geometric.hpp>



acgm::PointLight::PointLight(float intensity, glm::vec3 position, float range, float linAtt, float quadAtt)
	:acgm::Light(intensity), m_position(position), m_range(range), m_linAtt(linAtt), m_quadAtt(quadAtt)
{
}

float acgm::PointLight::GetIntensityInPoint(const glm::vec3 & point) const
{
	float l, q;
	float dist = GetDistanceToLight(point);

	//linear and quadratic attenuation
	l = m_range / (m_range + dist * m_linAtt);
	q = (m_range * m_range) / (m_range * m_range + dist * dist * m_quadAtt);
	
	return (l * q * m_intensity);
}

glm::vec3 acgm::PointLight::GetDirectionToLight(const glm::vec3 & point) const
{
	return glm::normalize(m_position - point);
}

float acgm::PointLight::GetDistanceToLight(const glm::vec3 & point) const
{
	return glm::length(m_position - point);
}
