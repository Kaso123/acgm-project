#include <ACGM_RayTracer_lib/CheckerShader.h>
#include <glm/common.hpp>



acgm::CheckerShader::CheckerShader(float cube, std::shared_ptr<acgm::Shader> shader0, std::shared_ptr<acgm::Shader> shader1)
	: m_cubeSize(cube), m_shader0(shader0), m_shader1(shader1), m_bias(0.0001f)
{
}

cogs::Color3f acgm::CheckerShader::CalculateColor(const acgm::ShaderInput& input)
{
	// (floor(x/s + b) + fl(y/s + b) + fl(z/s +b)) mod 2
	if(glm::mod((glm::floor(input.point.x/m_cubeSize + m_bias)+ glm::floor(input.point.y / m_cubeSize + m_bias)+ glm::floor(input.point.z / m_cubeSize + m_bias)),2.0f))
		return m_shader1->CalculateColor(input); //result 1
	return m_shader0->CalculateColor(input); //result 0
}



const float acgm::CheckerShader::Glossiness(glm::vec3 point) const
{
	// (floor(x/s + b) + fl(y/s + b) + fl(z/s +b)) mod 2
	if (glm::mod((glm::floor(point.x / m_cubeSize + m_bias) + glm::floor(point.y / m_cubeSize + m_bias) + glm::floor(point.z / m_cubeSize + m_bias)), 2.0f))
		return m_shader1->Glossiness(point); //result 1
	return m_shader0->Glossiness(point); //result 0
}

const float acgm::CheckerShader::Transparency(glm::vec3 point) const
{
	// (floor(x/s + b) + fl(y/s + b) + fl(z/s +b)) mod 2
	if (glm::mod((glm::floor(point.x / m_cubeSize + m_bias) + glm::floor(point.y / m_cubeSize + m_bias) + glm::floor(point.z / m_cubeSize + m_bias)), 2.0f))
		return m_shader1->Transparency(point); //result 1
	return m_shader0->Transparency(point); //result 0
}

const float acgm::CheckerShader::RefractionIndex(glm::vec3 point) const
{
	// (floor(x/s + b) + fl(y/s + b) + fl(z/s +b)) mod 2
	if (glm::mod((glm::floor(point.x / m_cubeSize + m_bias) + glm::floor(point.y / m_cubeSize + m_bias) + glm::floor(point.z / m_cubeSize + m_bias)), 2.0f))
		return m_shader1->RefractionIndex(point); //result 1
	return m_shader0->RefractionIndex(point); //result 0
}

