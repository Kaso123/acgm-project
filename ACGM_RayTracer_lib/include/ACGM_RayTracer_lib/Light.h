#pragma once
#include <glm/vec3.hpp>


namespace acgm
{
  //! Light  - abstract base class for scene lights
  class Light
  {
  public:
    explicit Light(float intensity);
    virtual ~Light() = default;

	//! Return light intensity in specified point
	virtual float GetIntensityInPoint(const glm::vec3& point) const = 0;

	//! Return normalized direction vector from light to point
	virtual glm::vec3 GetDirectionToLight(const glm::vec3& point) const = 0;

	//! Return distance between light and point
	virtual float GetDistanceToLight(const glm::vec3& point) const = 0;

  protected:
	 float m_intensity;
  };
}
