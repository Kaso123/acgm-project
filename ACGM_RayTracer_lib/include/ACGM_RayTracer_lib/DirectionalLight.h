#pragma once
#include <ACGM_RayTracer_lib/Light.h>

namespace acgm
{
  //! DirectionalLight
  class DirectionalLight : public acgm::Light
  {
  public:
    DirectionalLight(float intensity,glm::vec3 direction);

	float GetIntensityInPoint(const glm::vec3& point) const override;
	glm::vec3 GetDirectionToLight(const glm::vec3& point) const override;
	float GetDistanceToLight(const glm::vec3& point) const override;

  private:
	 glm::vec3 m_direction;
  };
}
