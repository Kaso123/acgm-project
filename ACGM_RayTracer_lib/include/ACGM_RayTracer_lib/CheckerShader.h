#pragma once

#include <ACGM_RayTracer_lib/Shader.h>



namespace acgm
{
  //! CheckerShader  - consists of 2 shaders
  class CheckerShader: public acgm::Shader
  {
  public:
    CheckerShader(float cube, std::shared_ptr<acgm::Shader> shader0, std::shared_ptr<acgm::Shader> shader1);
    virtual ~CheckerShader() = default;

	cogs::Color3f CalculateColor(const acgm::ShaderInput& input) override;
	const float Glossiness(glm::vec3 point) const override;
	const float Transparency(glm::vec3 point) const override;
	const float RefractionIndex(glm::vec3 point) const override;
	
  private:
	  float m_bias;
	  float m_cubeSize;
	  std::shared_ptr<acgm::Shader> m_shader0;
	  std::shared_ptr<acgm::Shader> m_shader1;
  };
}
