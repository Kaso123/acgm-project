#pragma once

#include <ACGM_RayTracer_lib/Shader.h>



namespace acgm
{
  //! PhongShader  - using ambient, diffuse, Blin-Phong specular
  class PhongShader : public acgm::Shader
  {
  public:
    PhongShader(cogs::Color3f color, float shininess, float ambient, float diffuse, float specular, float gloss, float trans, float refrIndex);
    virtual ~PhongShader() = default;
	
	cogs::Color3f CalculateColor(const acgm::ShaderInput& input) override;
	const float Glossiness(glm::vec3 point) const override { return m_gloss; };
	const float Transparency(glm::vec3 point) const override { return m_trans; };
	const float RefractionIndex(glm::vec3 point) const override { return m_refrIndex; };

  private:
	  //helper function to get angle between 2 vectors in radians
	  float GetAngleBetween(const glm::vec3& dir1, const glm::vec3& dir2) const;

	  cogs::Color3f m_color;
	  float m_shininess;
	  float m_ambient;
	  float m_diffuse;
	  float m_specular;
	  float m_gloss;
	  float m_trans;
	  float m_refrIndex;
  };
}
