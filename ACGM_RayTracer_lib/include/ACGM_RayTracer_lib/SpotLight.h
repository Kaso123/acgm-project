#pragma once
#include <ACGM_RayTracer_lib/PointLight.h>

namespace acgm
{
  //! Spot Light  - base point light + cone
  class SpotLight : public acgm::PointLight
  {
  public:
    SpotLight(float intensity, glm::vec3 position, float range, float linAtt, float quadAtt, glm::vec3 direction, float cutoff, float exponent);
    virtual ~SpotLight() = default;

	float GetIntensityInPoint(const glm::vec3& point) const override;

  private:

	  //! Helper function to calculate angle between 2 vectors in degrees
	  float GetAngleBetween(const glm::vec3& dir1, const glm::vec3& dir2) const;

	  glm::vec3 m_direction;
	  float m_cutoff_angle;
	  float m_exponent;
  };
}
