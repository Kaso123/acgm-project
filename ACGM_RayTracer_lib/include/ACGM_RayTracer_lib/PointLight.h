#pragma once
#include <ACGM_RayTracer_lib/Light.h>


namespace acgm
{
  //! Point Light
  class PointLight : public acgm::Light
  {
  public:
    PointLight(float intensity, glm::vec3 position, float range, float linAtt, float quadAtt);

	virtual float GetIntensityInPoint(const glm::vec3& point) const override;
	glm::vec3 GetDirectionToLight(const glm::vec3& point) const override;
	float GetDistanceToLight(const glm::vec3& point) const override;

  private:
	  float m_range;
	  float m_linAtt;
	  float m_quadAtt;

	  glm::vec3 m_position;
  };
}
