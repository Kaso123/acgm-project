#pragma once

#include <ACGM_RayTracer_lib/Model.h>
#include <glm/vec3.hpp>

namespace acgm
{
  //! Plane representation
  class Plane : public Model
  {
  public:
	  Plane(const glm::vec3& normal,const glm::vec3& point);
	  std::optional<acgm::HitResult> Intersect(const acgm::Ray& ray) const override;

  private:
	  glm::vec3 m_normal;
	  glm::vec3 m_point;
  };
}
