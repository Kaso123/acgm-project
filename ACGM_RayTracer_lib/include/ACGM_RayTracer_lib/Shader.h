#pragma once

#include <COGS/Color.h>
#include <ACGM_RayTracer_lib/Ray.h>



namespace acgm
{
	struct ShaderInput {
		glm::vec3 point;
		glm::vec3 normal;
		glm::vec3 dirToCamera;
		glm::vec3 dirToLight;
		float intensity;
		bool shadow;
	};

  //! Shader  - abstract base class for shaders
  class Shader
  {
  public:
    explicit Shader();
    virtual ~Shader() = default;

	virtual cogs::Color3f CalculateColor(const ShaderInput& input) = 0;
	virtual const float Glossiness(glm::vec3 point) const = 0;
	virtual const float Transparency(glm::vec3 point) const = 0;
	virtual const float RefractionIndex(glm::vec3 point) const = 0;
  private:
    
  };
}
