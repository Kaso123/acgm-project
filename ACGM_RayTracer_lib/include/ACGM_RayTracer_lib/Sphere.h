#pragma once
#include <ACGM_RayTracer_lib/Model.h>



namespace acgm
{
  class Sphere : public acgm::Model
  {
  public:
    Sphere(const glm::vec3 &position, float radius);
    virtual ~Sphere() = default;
    std::optional<acgm::HitResult> Intersect(const acgm::Ray &ray) const override;
    const glm::vec3 &GetPosition() const;
    float GetRadius() const;
  private:
    glm::vec3 m_position;
    float m_radius;
  };
}
