#pragma once
#include <string>
#include <COGS/Color.h>

namespace acgm
{
  //! Image representation
  class Image
  {
  public:
	  Image(std::string& file);
	  ~Image();

	  //! Get color at UV coordinates
	  cogs::Color3f GetColorAt(const glm::vec2 &uvs) const;

  private:
	  
	  //! Get pixel value <0,height/width> from uv value <0,1>
	  int UvToInt(float num, bool x = true) const;
	  
	  uint8_t* m_image;
	  int m_bpp;
	  int m_width;
	  int m_height;
  };
}
