#pragma once

#include <ACGM_RayTracer_lib/Model.h>
#include <glm/vec3.hpp>
#include <COGS/Mesh.h>
#include <COGS/Transform.h>

namespace acgm
{
	//! Mesh representation
	class Mesh : public Model
	{
	public:
		Mesh() {};
		Mesh(const std::string& filename,const glm::mat4& transform);
		std::optional<acgm::HitResult> Intersect(const acgm::Ray& ray) const override;
		
		//! - Intersection with single triangle
		std::optional<acgm::HitResult> TriangleIntersect(const acgm::Ray& ray, const glm::vec3& a, const glm::vec3& b, const glm::vec3& c) const;
	private:
		cogs::Mesh m_mesh;
		cogs::Transform m_transformMatrix;
	};
}