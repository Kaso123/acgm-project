#pragma once

#include <glm/common.hpp>


namespace acgm
{
  //! A Camera representation
  class Camera
  {
  public:
    //! Camera constructor
    Camera(const glm::vec3 &position, const glm::vec3 &up_direction, const glm::vec3 &forward_direction,
		float z_near, float z_far, float fov_y_rad);

    //! Get camera position
    const glm::vec3 &Position() const;
	glm::vec3 Direction() const;
	glm::vec3 UpVector() const;
	glm::vec3 RightVector() const;
	const float FovX() const { return m_fovX; };
	const float FovY() const { return m_fovY; };
	const float FarClippingPlane() { return m_farClippingPlane; };
	const float NearClippingPlane() { return m_nearClippingPlane; };

  private:
    //! Position in the scene
    const glm::vec3 m_position;
    //! Target - the point the camera looks at
    const glm::vec3 m_upVector;
	const glm::vec3 m_forwardVector;
	float m_fovX;
	float m_fovY;

	float m_farClippingPlane;
	float m_nearClippingPlane;

    // #TODO Add other camera parameters
  };
}
