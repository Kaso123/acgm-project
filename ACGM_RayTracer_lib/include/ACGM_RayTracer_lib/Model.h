#pragma once

#include <COGS/Color.h>
#include <ACGM_RayTracer_lib/Ray.h>
#include <ACGM_RayTracer_lib/Shader.h>
#include <optional>


namespace acgm
{
	struct ShaderInput;

	struct HitResult {
		float dist; //distance from camera
		glm::vec3 normal; //normal of hit surface
		std::shared_ptr<acgm::Model> model;
	};


  //! Model  - abstract base class for scene models
  class Model
  {
  public:
    explicit Model();
    virtual ~Model() = default;
	virtual std::optional<acgm::HitResult> Intersect(const acgm::Ray& ray) const = 0;

	void SetShader(std::shared_ptr<acgm::Shader> shader) { m_shader = shader; };
	void SetName(std::string name) { m_name = name; };

	//! Get color from shader
	const cogs::Color3f Color(acgm::ShaderInput& input) const;
	float Glossiness(glm::vec3 point) const { return m_shader->Glossiness(point); };
	float Transparency(glm::vec3 point) const { return m_shader->Transparency(point); };
	float RefractionIndex(glm::vec3 point) const { return m_shader->RefractionIndex(point); };

  private:
	std::string m_name;
	std::shared_ptr<acgm::Shader> m_shader;
  };
}
