#pragma once

#include <glm/vec3.hpp>
#include <glm/gtc/constants.hpp>
#include <glm/geometric.hpp>

namespace acgm
{
  class Plane;
  class Model;

  //! Representation of a ray
  class Ray
  {
  public:
	  Ray(glm::vec3 origin, glm::vec3 direction);

	  const glm::vec3& Origin() const { return m_origin; };
	  const glm::vec3& Direction() const { return m_direction; };
	  const float GetBias() const { return 0.0001f; }; //constant for now

	  //! Get point on ray in specific distance from origin
	  const glm::vec3 GetPoint(float dist) const;
  private:
	  glm::vec3 m_origin;
	  glm::vec3 m_direction;	
  };
}
