#pragma once

#include <vector>
#include <HIRO_DRAW/renderers/RasterRenderer.h>


namespace acgm
{
	class Model;
	class Camera;
	class Light;
	class Ray;
	class Image;

  //! Scene representation
  class Scene
  {
  public:
    //Initialize a Scene object
    Scene(std::shared_ptr<acgm::Camera> camera, std::shared_ptr<acgm::Light> light, std::vector<std::shared_ptr<acgm::Model>> models, 
		std::string& image_file, glm::vec3 envUp, glm::vec3 envSeam, float airRefrIndex);
    ~Scene() = default;

    //! RayTrace the scene
    void Raytrace(hiro::draw::RasterRenderer &renderer, int maxRefl, int maxRefr) const;

	//! Create reflection ray from incoming ray
	std::shared_ptr<acgm::Ray> ReflectionRay(glm::vec3 ray, glm::vec3 normal, glm::vec3 origin) const;

	//! Create refraction ray from incoming ray
	std::shared_ptr<acgm::Ray> RefractionRay(glm::vec3 ray, glm::vec3 normal, glm::vec3 origin, float refrIndex) const;

  private:
	  //! Create ray from scene camera for selected pixel
	  std::shared_ptr<acgm::Ray> CameraRay(int x, int y, std::shared_ptr<acgm::Camera> camera, int imageWidth, int imageHeight) const;

	  //! Recursively cast reflection and refraction rays
	  cogs::Color3f RecursiveCastRay(std::shared_ptr<acgm::Ray> ray, int reflCount, int refrCount, int maxRefl, int maxRefr) const;

	  //! Get UVs from ray (for environment map)
	  glm::vec2 GetUVs(std::shared_ptr<acgm::Ray> ray) const;

	  //scene objects
	  std::shared_ptr<acgm::Camera> m_camera;
	  std::vector<std::shared_ptr<acgm::Model>> m_models;
	  std::shared_ptr<acgm::Light> m_light;
	  std::shared_ptr<acgm::Image> m_image;

	  glm::vec3 m_envUp;
	  glm::vec3 m_envSeam;

	  float m_refractionIndex;
  };
}
